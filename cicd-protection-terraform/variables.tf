variable "gitlab_base_url" {
  type    = string
  default = "https://gitlab.com"
}

variable "gitlab_token" {
  type      = string
  sensitive = true
}

variable "my_group_id" {
  type      = string
  default   = "82137824" # placeholder
}

variable "external_api_key_name" {
  type      = string
  default   = "EXTERNAL_API_KEY"
}

variable "external_api_key" {
  type      = string
  sensitive = true
  default   = "some3xtraRand0mString"
}
