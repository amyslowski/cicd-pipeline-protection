resource "gitlab_group" "cicd_tf" {
  name                      = "cicd-protection-tf"
  path                      = "cicd-protection-tf"
  parent_id                 = var.my_group_id
  description               = "Terraform managed group"
  visibility_level          = "private"
  default_branch_protection = 2
  project_creation_level    = "maintainer"
}

resource "gitlab_group_variable" "cicd_tf_var_api_key" {
  group             = gitlab_group.cicd_tf.id
  key               = var.external_api_key_name
  value             = var.external_api_key
  protected         = true
  masked            = true
  environment_scope = "*"
}

resource "gitlab_project" "cicd_workshop_tf" {
  name                                             = "cicd-workshop-tf"
  namespace_id                                     = gitlab_group.cicd_tf.id
  description                                      = "Terraform managed workshop repository"
  topics                                           = ["tf", "iac", "workshop"]
  visibility_level                                 = "private"
  default_branch                                   = "develop"
  squash_option                                    = "default_on"
  only_allow_merge_if_pipeline_succeeds            = true
  only_allow_merge_if_all_discussions_are_resolved = true
  # pipelines_enabled                                = true
  public_jobs                                      = false
}

resource "gitlab_branch" "develop" {
  name    = "develop"
  ref     = "main"
  project = gitlab_project.cicd_workshop_tf.id

  depends_on = [gitlab_project.cicd_workshop_tf]
}

resource "gitlab_branch_protection" "develop_protected" {
  project                      = gitlab_project.cicd_workshop_tf.id
  branch                       = "develop"
  push_access_level            = "developer"
  merge_access_level           = "maintainer"
  unprotect_access_level       = "maintainer"
  allow_force_push             = false
  code_owner_approval_required = false

  depends_on = [gitlab_branch.develop]
}

resource "gitlab_repository_file" "gitlab_ci" {
  project        = gitlab_project.cicd_workshop_tf.id
  branch         = "develop"
  file_path      = ".gitlab-ci.yml"
  commit_message = "Adding gitlab-ci.yml"
  encoding       = "text"
  content        = <<EOF
include:
  - template: Jobs/Secret-Detection.gitlab-ci.yml

variables:
  PASSWORD: H!gh.3NtR0py.StR1nG
  GITLAB_TOKEN: glpat-MxK-C-mdfBXG_pS346Ng

stages:
  - test
  - my_stage

my_cicd:
  stage: my_stage
  script:
    - env
  EOF

  depends_on = [gitlab_branch_protection.develop_protected]
}

# User added as owner whose token was used to create resources
# resource "gitlab_project_membership" "frost273" {
#   project      = gitlab_project.cicd_tf.id
#   user_id      = 19632541
#   access_level = "owner"
# }
