data "gitlab_user" "allowed_to_push_main" {
  count    = length(var.allowed_to_push_main)
  username = var.allowed_to_push_main[count.index]
}

data "gitlab_user" "allowed_to_merge_main" {
  count    = length(var.allowed_to_merge_main)
  username = var.allowed_to_merge_main[count.index]
}

data "gitlab_user" "allowed_to_merge_develop" {
  count    = length(var.allowed_to_merge_develop)
  username = var.allowed_to_merge_develop[count.index]
}

data "gitlab_user" "allowed_to_push_develop" {
  count    = length(var.allowed_to_push_develop)
  username = var.allowed_to_push_develop[count.index]
}

data "gitlab_user" "allowed_to_unprotect_develop" {
  count    = length(var.allowed_to_unprotect_develop)
  username = var.allowed_to_unprotect_develop[count.index]
}
