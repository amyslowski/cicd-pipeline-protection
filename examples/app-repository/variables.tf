variable "project_name" {
  type        = string
  description = "The name of the project"
}
variable "project_description" {
  type        = string
  description = "A description of the project"
}
variable "namespace_id" {
  type        = number
  default     = 1111
  description = "The namespace (sub-group id) of the project"
}
variable "default_branch" {
  type        = string
  default     = "develop"
  description = "The default branch for the project"
}
variable "initialize_with_readme" {
  type        = bool
  default     = false
  description = "Create main branch with first commit containing a README.md file"
}
variable "visibility_level" {
  type        = string
  default     = "internal"
  description = "A project in GitLab can be private, internal, or public"
}
variable "approvals_before_merge" {
  type        = number
  default     = 2
  description = "Number of merge request approvals required for merging"
}
variable "author_email_regex" {
  type        = string
  default     = "@epam\\.com$"
  description = "All commit author emails must match this regex"
}

variable "commit_message_regex" {
  type        = string
  default     = null
  description = "All commit messages must match this regex"
}

variable "predefined_commit_message_regex_name" {
  type        = string
  default     = "ticket_required"
  description = "A regexes defined in commit_message_regexes_map"
}

variable "commit_message_regexes_map" {
  type        = map(string)
  description = "All commit messages must match one of these regexes"
  default = {
    ticket_required = "Add README.md|(?-m)^(chore|docs|merge|build|ci|feat|fix|perf|refactor|revert|test|qa|cd)(\\([aA-zZ]+\\-\\d+\\))(!)?: [^\\n\\r]+|(?-m)^(chore|docs|merge)(\\([A-Z]+\\-\\d+\\))?(!)?: [^\\n\\r]+"
    ticket_optional = "Add README.md|(?-m)^(build|chore|ci|docs|feat|fix|perf|refactor|revert|test|qa|cd|merge)(\\([A-Z]+\\-\\d+\\))?(!)?: [^\\n\\r]+"
  }
}

variable "merge_commit_template" {
  type        = string
  default     = "merge: branch '%%{source_branch}' into '%%{target_branch}'\n\n%%{title}\n\n%%{issues}\n\nSee merge request %%{reference}"
  description = "Template used to create merge commit message in merge requests"
}

variable "squash_commit_template" {
  type        = string
  default     = "%%{title}"
  description = "Template used to create squash commit message in merge requests"
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  type        = bool
  default     = true
  description = "Set to true if you want allow merges only if all discussions are resolved"
}

variable "only_allow_merge_if_pipeline_succeeds" {
  type        = bool
  default     = true
  description = "Set to true if you want allow merges only if a pipeline succeeds"
}

variable "printing_merge_request_link_enabled" {
  type        = bool
  default     = true
  description = "Show link to create/view merge request when pushing from the command line"
}

variable "remove_source_branch_after_merge" {
  type        = bool
  default     = true
  description = "Enable Delete source branch option by default for all new merge requests"
}

variable "request_access_enabled" {
  type        = bool
  default     = true
  description = "Allow users to request member access"
}

variable "gitlab_token" {
  type    = string
  default = ""
}

variable "merge_method" {
  type        = string
  default     = "merge"
  description = "There are three options: merge, rebase_merge and ff"
}

variable "squash_option" {
  type        = string
  default     = "default_off"
  description = "Squash commits when merge request. Valid values are never, always, default_on, or default_off"
}

variable "allowed_to_merge_develop" {
  description = "Defines permissions for action. Takes a list of usernames."
  default     = []
  type        = list(string)
}

variable "allowed_to_push_develop" {
  description = "Defines permissions for action. Takes a list of usernames."
  default     = []
  type        = list(string)
}

variable "allowed_to_unprotect_develop" {
  description = "Defines permissions for action. Takes a list of usernames."
  default     = []
  type        = list(string)
}

variable "allowed_to_merge_main" {
  description = "Defines permissions for action. Takes a list of usernames."
  default     = []
  type        = list(string)
}

variable "allowed_to_push_main" {
  description = "Defines who's able to push straight to the main branch. Takes a list of usernames."
  default     = []
  type        = list(string)
}

### Special variables for manually created repositories in the past
variable "imported" {
  type        = bool
  default     = false
  description = "Was this project created manually before terraform or not"
}

variable "imported_main_ref" {
  type        = string
  default     = ""
  description = "Specify SHA sum of the HEAD of main branch. Needs for importing an already existing branch"
}

variable "topics" {
  type        = set(string)
  default     = null
  description = "The list of topics for the project."
}

variable "merge_requests_template" {
  type        = string
  default     = ""
  description = "Sets the template for new merge requests in the project."
}

variable "allow_merge_on_skipped_pipeline" {
  type        = bool
  default     = true
  description = "Set to true if you want to treat skipped pipelines as if they finished with success."
}

variable "auto_devops_enabled" {
  type        = bool
  default     = false
  description = "Enable Auto DevOps for this project."
}

variable "additional_members" {
  type        = map(any)
  default     = {}
  description = "Additional username project's members with access_level."
}

variable "reset_approvals_on_push" {
  type        = bool
  default     = true
  description = "Set to true if you want to remove all approvals in a merge request when new commits are pushed to its source branch."
}

variable "disable_overriding_approvers_per_merge_request" {
  type        = bool
  default     = true
  description = "By default, users are able to edit the approval rules in merge requests"
}

variable "merge_requests_author_approval" {
  type        = bool
  default     = false
  description = "Set to true if you want to allow merge request authors to self-approve merge requests"
}

variable "merge_requests_disable_committers_approval" {
  type        = bool
  default     = true
  description = "Set to true if you want to prevent approval of merge requests by merge request committers."
}

variable "code_owner_approval_4main" {
  type        = bool
  default     = false
  description = "Can be set to true to require code owner approval before merging to main"
}

variable "code_owner_approval_4default" {
  type        = bool
  default     = false
  description = "Can be set to true to require code owner approval before merging to a default branch"
}

variable "branch_name_regex" {
  type        = string
  default     = "^[\\w\\-\\/\\.]{1,45}$"
  description = "All branch names must match this regex."
}

variable "gitlab_base_url" {
  type        = string
  default     = "https://gitlab.com/"
  description = "This is the target GitLab HTTP(S) FQDN endpoint."
}

variable "archived" {
  type        = bool
  default     = false
  description = "Whether the project is in read-only mode (archived)"
}

variable "file_name_regex" {
  type        = string
  default     = ""
  description = "All committed filenames must not match this regex, e.g. (jar|exe)$."
}

variable "merge_access_level_main" {
  type        = string
  default     = "developer"
  description = "Access level for merging into the main branch"
}
