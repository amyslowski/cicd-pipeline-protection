module "service_user_lookup" {
  source = "../gitlab-service-user"
}

module "gitlab_users_lookup" {
  source = "../gitlab-users"
  users  = keys(var.additional_members)
}

resource "gitlab_project" "project" {
  name        = var.project_name
  description = var.project_description

  approvals_before_merge                           = var.approvals_before_merge
  default_branch                                   = var.default_branch
  initialize_with_readme                           = var.initialize_with_readme
  merge_commit_template                            = var.merge_commit_template
  namespace_id                                     = var.namespace_id
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  printing_merge_request_link_enabled              = var.printing_merge_request_link_enabled
  remove_source_branch_after_merge                 = var.remove_source_branch_after_merge
  request_access_enabled                           = var.request_access_enabled
  squash_commit_template                           = var.squash_commit_template
  visibility_level                                 = var.visibility_level
  squash_option                                    = var.squash_option
  merge_method                                     = var.merge_method
  topics                                           = var.topics
  merge_requests_template                          = var.merge_requests_template
  allow_merge_on_skipped_pipeline                  = var.allow_merge_on_skipped_pipeline
  auto_devops_enabled                              = var.auto_devops_enabled
  archived                                         = var.archived

  push_rules {
    author_email_regex   = var.author_email_regex
    branch_name_regex    = var.branch_name_regex
    commit_message_regex = var.commit_message_regex != null ? var.commit_message_regex : var.commit_message_regexes_map[var.predefined_commit_message_regex_name]
    file_name_regex      = var.file_name_regex
  }

  provisioner "local-exec" {
    command = "sleep 5 && curl --fail --header \"PRIVATE-TOKEN: ${var.gitlab_token}\" -X DELETE ${var.gitlab_base_url}/api/v4/projects/${gitlab_project.project.id}/protected_branches/${var.default_branch}"
  }

  lifecycle {
    prevent_destroy = true
  }

}

resource "gitlab_project_level_mr_approvals" "project" {
  project                                        = gitlab_project.project.id
  reset_approvals_on_push                        = var.reset_approvals_on_push
  disable_overriding_approvers_per_merge_request = var.disable_overriding_approvers_per_merge_request
  merge_requests_author_approval                 = var.merge_requests_author_approval
  merge_requests_disable_committers_approval     = var.merge_requests_disable_committers_approval
}

resource "gitlab_branch" "main" {

  name    = "main"
  ref     = var.imported ? var.imported_main_ref : var.default_branch
  project = gitlab_project.project.id
}

resource "gitlab_branch_protection" "main" {

  project                      = gitlab_project.project.id
  branch                       = gitlab_branch.main.name
  allow_force_push             = false
  code_owner_approval_required = var.code_owner_approval_4main

  push_access_level      = "no one"
  merge_access_level     = var.merge_access_level_main
  unprotect_access_level = "maintainer"

  dynamic "allowed_to_push" {
    for_each = flatten([module.service_user_lookup.srv-user.id, data.gitlab_user.allowed_to_push_main[*].id])
    content {
      user_id = allowed_to_push.value
    }
  }

  dynamic "allowed_to_merge" {
    for_each = data.gitlab_user.allowed_to_merge_main[*].id
    content {
      user_id = allowed_to_merge.value
    }
  }

  depends_on = [time_sleep.wait_90_seconds]
}

# Since gitlab from time to time work exceptionally slow, this is a workaround to let all changes to propagate.
resource "time_sleep" "wait_90_seconds" {
  create_duration = "90s"
}

resource "gitlab_branch_protection" "default" {
  project                      = gitlab_project.project.id
  branch                       = gitlab_project.project.default_branch
  allow_force_push             = false
  code_owner_approval_required = var.code_owner_approval_4default

  push_access_level      = "no one"
  merge_access_level     = "developer"
  unprotect_access_level = "maintainer"

  dynamic "allowed_to_merge" {
    for_each = data.gitlab_user.allowed_to_merge_develop[*].id
    content {
      user_id = allowed_to_merge.value
    }
  }

  dynamic "allowed_to_push" {
    for_each = data.gitlab_user.allowed_to_push_develop[*].id
    content {
      user_id = allowed_to_push.value
    }
  }

  dynamic "allowed_to_unprotect" {
    for_each = data.gitlab_user.allowed_to_unprotect_develop[*].id
    content {
      user_id = allowed_to_unprotect.value
    }
  }
}

resource "gitlab_project_membership" "additional_members" {
  for_each     = var.additional_members
  project      = gitlab_project.project.id
  user_id      = module.gitlab_users_lookup.users[each.key].id
  access_level = each.value
}
