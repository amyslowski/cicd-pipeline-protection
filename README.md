# Upskill

## CI/CD Pipeline Protection - January 2024

My notes + examples of code + everything else related


### Init user
1. Create user account at Gitlab.com
1. Check Gitlab version
1. Create Personal access token, set expiration date - v15.4+ if expiration is not set: is 1 year
1. Use access token to access API (optional)

### Init repository
1. Create a group, name should be unique, since its being created in general namespace - avoid using sensitive data in names
1. Create subgroup (optional) - namespace is private
1. Create group variable - masked and protected.

Note:
Group - Settings: 
role to create projects - maintainers
roles allowed to create subgroups - owners
create group variable (EXTERNAL_API_KEY) mask + protect
https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable
The @, :, ., or ~ characters. Other special characters will not comply with masking attribute option.

### Create repository
1. Create project
1. Add topics: workshop, cicd
1. New branch: develop
1. Protect branch: develop


### Merge requests
remove approvals on commit
prevent author approval
prevent approvalfor  user who add commits
prevent editing rules

### Push rules
signed committers only
prevent pushing secret files

### Add file .gitlab-ci.yml
```yml
include:
  - template: Jobs/Secret-Detection.gitlab-ci.yml

variables:
  PASSWORD: H!gh.3NtR0py.StR1nG
  GITLAB_TOKEN: glpat-MxK-C-mdfBXG_pS346Ng

stages:
  - test
  - my_stage

my_cicd:
  stage: my_stage
  script:
    - env
```

https://gitlab.com/workshop-tf/cicd-protection-tf/cicd-workshop-tf/-/blob/develop/.gitlab-ci.yml?ref_type=heads
https://gitlab.com/workshop-tf/cicd-protection-tf/cicd-workshop-tf/-/jobs/6110835224
https://gitlab.com/workshop-tf/cicd-protection-tf/cicd-workshop-tf/-/jobs/6110835226
https://gitlab.com/workshop-tf/cicd-protection-tf/cicd-workshop-tf/-/artifacts

https://gitlab.com/workshop-tf/cicd-protection-tf/cicd-workshop-tf/-/ci/editor?branch_name=develop
https://gitlab.com/gitlab-org/gitlab/-/raw/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml
https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml?ref_type=heads

https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/cicd/templates.md

https://docs.gitlab.com/ee/user/application_security/secret_detection/
https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml

runners
register runners for lowest practical level - example runner for subgroup than rules applicable for sub sub group and for POC projects
require tags
cicd variables at lowest practical level - project or even env level variables
use protected runners with protected variables and protected branches - limit 

## Terraform - IaC
export TF_LOG=debug

terraform init
terraform validate
terraform fmt
export TF_VAR_gitlab_token=**** # no default value in variables
terraform import gitlab_project.cicd_tf 54130527 # resource_name id
terraform plan
terraform apply
terraform destroy



TO ADD:
strategic use of branch protection rules,
auto-merge rules, and access control fortifies CI/CD security. 


Best practices:
Group level settings ? to cascade into sub-groups
Visibility: Private or internal (no access without explicit membership)
SAML SSO
Complience reports ? https://gitlab.kfplc.com/groups/SFD/FFX/-/security/compliance_dashboard/violations
Establish periodical review process - monitor them for unusual activity
use protected runners with protected variables and protected branches - avoid misuse of cloud resources
.gitlab-ci controlled by CODEOWNERS

Doubtable
Allow project and group access token creation. Project and group access tokens are much like personal access tokens with the following improvements:

    They are visible to and manageable by group owners and maintainers, which means they can be revoked and have expiration dates set by an administrator to limit the opportunity for abuse.
    They create a virtual “bot” user that does not count against your license count.

---
## References:
https://en.wikipedia.org/wiki/Defense_in_depth_(computing)  
https://en.wikipedia.org/wiki/Infrastructure_as_code  
  
https://about.gitlab.com/blog/2023/05/31/securing-your-code-on-gitlab/  
https://gitlab.com/gitlab-org/gitlab/-/pipelines (who know more about ci/cd than gitlab itself)  
  
https://owasp.org/www-project-top-ten/  
https://owasp.org/www-project-top-10-ci-cd-security-risks/  
  
https://gitlab.com/gitlab-learn-labs/  
https://docs.gitlab.com/ee/api/rest/  
  
https://registry.terraform.io/providers/gitlabhq/gitlab/latest  
